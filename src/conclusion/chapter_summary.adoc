== Vorgehensmodelle im Vergleich
Das folgende Kapitel bietet eine Übersicht der wichtigsten Eigenschaften der im vorherigen Teil der Arbeit vorgestellten Vorgehensmodelle. Zusätzlich werden die Vorgehen in Bezug auf ihre Anwendbarkeit in großen Teams analysiert und die Vor- und Nachteile dieser Modelle untersucht. Durch diese Übersicht werden die Grundlagen gelegt, um Kriterien zu identifizieren, anhand derer ein Vergleich und eine Entscheidungshilfe bei der Auswahl eines geeigneten Vorgehensmodells ermöglicht wird.

Das Feature-Branching-Modell ist eines der gebräuchlichsten Vorgehensmodelle bei der Verwendung von Git in der Software-Entwicklung. Dabei werden neue Funktionen oder Änderungen in einem separaten Branch entwickelt, der von der Hauptentwicklungslinie (dem "Master"-Branch) abzweigt. Sobald die neue Funktion oder der Änderung bereit ist, wird sie in den Master-Branch integriert. 
Ein Vorteil dieses Modells ist, dass es die parallele Entwicklung von Funktionen ermöglicht und es einfach macht, Änderungen rückgängig zu machen. Ein Nachteil ist jedoch, dass es zu einer hohen Anzahl an Branches führen kann, die schwer zu verwalten sind. Dieses Problem wird besonders in sehr großen Teams prägnant ausgeprägt und mindert die Effizienz. Im Vergleich zu anderen Vorgehensmodellen zeichnet sich das Feature-Branching-Modell durch seine direkte und unkomplizierte Anwendung aus, was die Einarbeitungszeit verringern und die Verständlichkeit in der Kommunikation und Zusammenarbeit erhöhen kann. 

In dem Trunk-Based Development werden alle Änderungen direkt in den Master-Branch (dem "Trunk") integriert, ohne dass separate Branches verwendet werden. Dies ermöglicht schnelle Entwicklungszyklen und leichte Verfolgbarkeit von Änderungen, da es nur einen Branch gibt. Ein Nachteil von Trunk-Based Development ist, dass es schwieriger ist, parallele Entwicklungen zu verwalten und Konflikte entstehen können, wenn mehrere Entwickler gleichzeitig am gleichen Code arbeiten. Ein weiterer Nachteil ist, dass Funktionen im Vergleich zum Feature-Branch-Workflow nicht so atomar gekapselt werden können, was die Verfolgbarkeit des Entwicklungsfortschritts vermindert. Um diese Nachteile zu vermeiden, kann das Vorgehen durch das Hinzufügen von Feature-Branches erweitert werden, wodurch die Vorteile von Trunk-Based Development vermindert beibehalten werden und gleichzeitig die Kontrolle über parallele Entwicklungen behalten werden kann. Eine separate Behandlung von Entwicklungs- und Produktionsumgebung lässt sich nach dem Prinzipien des Vorgehens jedoch nicht umsetzten. Insgesamt bietet Trunk-Based Development eine schnelle, leicht verständliche und flexible Möglichkeit, die Entwicklung mit Git in Teams jeglicher Größe zu verwalten.

Der Gitflow-Workflow ist ein Vorgehensmodell, das auf dem Feature-Branching-Modell basiert und einige zusätzliche Regeln und Konzepte hinzufügt. Es verwendet separate Branches für neue Funktionen, Bugfixes und Releases, um die Entwicklung zu strukturieren und zu organisieren. Ein Vorteil dieses Modells ist, dass es eine klare Trennung zwischen verschiedenen Arten von Änderungen bietet und es leicht macht, Releases und Hotfixes zu verwalten. Dies wird von den beiden vorherig vorgestellten Vorgehen nicht zentral durch ein Prinzip gelöst. Ein Nachteil des Gitflow-Workflows ist, dass er komplexer und wartungsaufwändiger ist als andere Vorgehensmodelle und striktere Disziplin von den Entwicklern erfordert. Es kann auch eine höhere Lernkurve haben, da es einige zusätzliche Konzepte und Regeln enthält. Trotz dieser Nachteile bietet der Gitflow-Workflow jedoch eine strukturierte Möglichkeit, die Entwicklung mit Git in auch sehr großen Teams zu verwalten.

=== Entscheidungsfindung
Die in dieser Arbeit vorgestellten Git-Vorgehensmodelle bieten unterschiedliche Vorteile und Herausforderungen für verschiedene Teams und Anwendungsfälle. Die Entscheidung für das richtige Vorgehensmodell sollte daher sorgfältig abgewogen werden und an die individuellen Bedürfnisse des Teams angepasst sein. 

Um ein geeignetes Vorgehensmodell in der Software-Entwicklung mit Git zu finden und zu wählen, bietet die folgende Entscheidungsmatrix eine Hilfe.
In der Matrix werden die verschiedenen Git-Vorgehensmodelle anhand von Kriterien verglichen und individuell bewertet. Die Kriterien wurden auf der Basis der grundlegenden Unterschiede und den Anforderungen an moderne Software-Entwicklung ausgewählt.

********************************************
- *Trennung Produktionssystem/Entwicklungssystem*
- *Wartungsaufwand* umschreibt den Verwaltungs- und Lenkungsaufwand
- *Kognitiver Overhead* umschreibt einen dehnbaren Begriff, welcher die Komplexität der Struktur und den damit verbundenen Denkaufwand beschreibt, welcher für die präzise Anwendung des Vorgehens von Nöten ist
- *Skalierbarkeit* beschreibt die Möglichkeit, ob das Vorgehen ohne viel Aufwand gut auf sehr große Teams hochskaliert werden kann
- *Hoch Frequentives CD* definiert ein Kriterium, ob das zugrundliegende Projekt ständig in der Produktion auf dem aktuellsten Stand gehalten werden soll
********************************************

Mithilfe dieser Entscheidungsmatrix können die Vor- und Nachteile der verschiedenen Vorgehensmodelle berücksichtigt werden. Dies ermöglicht es, trotz unterschiedlicher Umgebungsfaktoren an das Projekt, eine fundierte Entscheidung für ein geeignetes Vorgehensmodell in der Software-Entwicklung mit Git zu treffen.

.Entscheidungsmatrix
[options="header"]
|================================================================================================================
| Kriterien                                       | Feature-Branching  | Trunk-based Development  | Gitflow      
| Trennung Produktions-/Entwicklungssystem   |                    |                          | X (Gegeben)  
| Wartungsaufwand                                 | X (Niedrig)        | X (Niedrig)              | X (Hoch)              
| Kognitiver Overhead                             | X (Gering)         | X (Gering)               | X (Gegeben)              
| Skalierbarkeit (Teamgröße)                      |                    | X (Skaliert)             | X (Skaliert) 
| Hoch Frequentives CD                            |                    | X (Hoch)                 |              
|================================================================================================================

Es lässt sich festhalten, dass der Feature-Branching Workflow durch seinen niedrigen Wartungsaufwand und den geringen kognitiven Overhead das am schnellsten anwendbare Vorgehensmodell darstellt. Dieses Modell ermöglicht jedoch keine separate Entwicklungs- und Produktionsumgebung und ist daher weniger geeignet für hoch frequente Produktionsumsetzungen. In sehr großen Teams kann es schnell zu Unorganisation führen, was die Effizienz der Produktentwicklung beeinträchtigt.

Das Trunk-based Development besitzt ebenfalls einen geringen Wartungsaufwand und kognitiven Overhead und ermöglicht durch die Zusammenführung aller Funktionalitäten auf den Trunk eine schnelle Veröffentlichung auf dem Produktionssystem. Durch die Möglichkeit, das Vorgehen durch Erweiterungen anzupassen, ist es gut skalierbar in sehr großen Teams.

Der Gitflow-Workflow trennt die Entwicklung zwischen Produktions- und Entwicklungssystem, was für manche Projekte von Vorteil sein kann. Allerdings erfordert dieses Vorgehen einen höheren Wartungsaufwand und kognitiven Overhead der Entwickler. Es ist jedoch gut skalierbar und kann ohne Einschränkungen in sehr großen Teams eingesetzt werden.